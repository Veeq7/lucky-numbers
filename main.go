package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func writeArrayToFile(arr []int, fileName string) {
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer file.Close()
	for _, number := range arr {
		file.WriteString(fmt.Sprint(number) + "\n")
	}
}

func readArrayFromFile(fileName string) []int {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	arr := make([]int, 0)
	for {
		str, err := reader.ReadString('\n')
		if err != nil {
			break
		}
		i, err := strconv.ParseInt(strings.Trim(str, " \r\n"), 10, 0)
		if err != nil {
			continue
		}
		arr = append(arr, int(i))
	}
	return arr
}

func generateAscendingArray(count int) []int {
	arr := make([]int, count)
	for n := range arr {
		arr[n] = n + 1
	}
	return arr
}

func removeEveryNth(slice []int, period int) []int {
	newSlice := make([]int, 0)
	for n, val := range slice {
		if n%period != period-1 {
			newSlice = append(newSlice, val)
		}
	}
	return newSlice
}

func getLuckyNumbers(count int) []int {
	arr := generateAscendingArray(count)
	i := 1
	lastValue := arr[i]
	for {
		arr = removeEveryNth(arr, arr[i])
		if arr[i] == lastValue {
			i++
		}
		if i >= len(arr) {
			break
		}
		lastValue = arr[i]
	}
	return arr
}

func intersection(arr1 []int, arr2 []int) []int {
	newArr := make([]int, 0)
	for _, val := range arr1 {
		if contains(arr2, val) {
			newArr = append(newArr, val)
		}
	}
	return newArr
}

func countLongestNumberStreak(arr []int) (int, int) {
	max := 0
	index := 0
	currentStreak := 0
	currentIndex := 0
	lastValue := arr[0]
	for n := range arr[1:] {
		if lastValue <= arr[n+1] {
			currentStreak++
			if max < currentStreak {
				max = currentStreak
				index = currentIndex
			}
		} else {
			currentStreak = 0
			currentIndex = n
		}
		lastValue = arr[n+1]
	}
	return max, index
}

func contains(slice []int, i int) bool {
	for _, val := range slice {
		if val == i {
			return true
		}
	}
	return false
}

func getMaxElem(slice []int) int {
	max := 0
	for _, val := range slice {
		if val > max {
			max = val
		}
	}
	return max
}

func removeNonPrime(slice []int, origin int) []int {
	newSlice := make([]int, 0)
	for _, val := range slice {
		if val == origin {
			newSlice = append(newSlice, val)
			continue
		}
		if val%origin != 0 {
			newSlice = append(newSlice, val)
		}
	}
	return newSlice
}

func generatePrimeSieve(size int) []int {
	arr := generateAscendingArray(size)[1:]

	n := 0
	lastValue := arr[n]
	for {
		arr = removeNonPrime(arr, arr[n])
		if arr[n] == lastValue {
			n++
		}
		if n >= len(arr) {
			break
		}
		lastValue = arr[n]
	}

	return arr
}

func countPrimeNumbers(arr []int) int {
	sieve := generatePrimeSieve(getMaxElem(arr))
	return len(intersection(arr, sieve))
}

const inputFile = "dane.txt"
const outputFile = "wyniki4.txt"

func main() {
	arr := readArrayFromFile(inputFile)
	file, err := os.Create("wyniki4.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	lucky := getLuckyNumbers(getMaxElem(arr))
	lucky = intersection(arr, lucky)
	file.WriteString(fmt.Sprintln("Number count:", len(arr)))
	file.WriteString(fmt.Sprintln("Lucky number count:", len(lucky)))
	//writeArrayToFile(lucky, outputFile)
	max, index := countLongestNumberStreak(arr)
	file.WriteString(fmt.Sprintln("Longest number streak index:", index))
	file.WriteString(fmt.Sprintln("Longest number streak:", max))
	file.WriteString(fmt.Sprintln("Prime Numbers:", countPrimeNumbers(arr)))
}
